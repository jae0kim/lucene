API Comare(using Lucene)
========================
#### 1. 기존 API 비교툴의 문제점
- 과거 Naver API 비교시 사용하던 API 비교 소스는 소스의 가독성이 떨어져 수정/재사용이 불가능
- DB에 API의 호출 결과를 저장하고 결과를 비교할때 Full Text 검색을 하여 자원 소모가 심각
- 툴을 필요로 하는 사용자가 아닌 개발자가 직접 기동해야 함 : 인력 자원 낭비

#### 2. 개선점
- Source Refactoring 이 쉽도록 전체 구조 개선(추가 개선이 쉬움)
- 변경이 잦은 Resource(API URI, Parameters)를 별도의 property/csv 파일로 분리
- 자원소모 개선
    - DB를 사용하지 않으므로 DB 자원 소모 없음
    - full Text 검색에 탁월한 Apache Lucene 사용하여 결과 비교시간 절약
    - excutable JAR 혹은 BAT파일로 편리한 실행환경을 제공한다면, 비개발자(실제 API비교 담당자)도 사용 가능

#### 3. Spec
- JDK 1.8
- Apache Lucene (인덱싱 / 검색 엔진)
- Apache HttpClient (Rest API Call)
- Google Gson (json process)
- HTTP METHOD : GET

#### 4. Guide
- API Info
    - com.jykim.apicompare.util.PropertiesUtil
    - api.properties 파일 편집
    - api name : 비교할 API의 이름으로 반드시 서로 달라야함
    - api uri : 비교 호출을 할 URI
    - key : 현재 ODsay LAB 기준으로 apiKey=xxx 로 붙음
        - Appletree를 사용한다면 com.jykim.apicompare.rest.RestService의 makeURI(int apiType)의 내용을 수정해야함
- parameter csv file
    - com.jykim.apicompare.util.ParametersUtil
    - 두 API에서 공통으로 사용할 파라미터 집합은 csv파일을 이용
    - 2개의 필드가 필요함 (parameter seq, get parameter 문자열)
    - 구분자는 ,(쉼표)
- Lucene Config
    - Indexed Filed을 저장할 Path 설정 필요
        - com.jykim.apicompare.document.LuceneProcessing 파일의 INDEX_PATH 상수값을 실행하는 사람의 Path에 맞게 수정해야함
    