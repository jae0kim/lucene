package com.jykim.apicompare;

import com.jykim.apicompare.document.LuceneProcessing;
import com.jykim.apicompare.rest.RestServcie;
import com.jykim.apicompare.util.ParametersUtil;
import com.jykim.apicompare.util.PropertiesUtil;
import com.jykim.apicompare.vo.ApiInfo;
import com.jykim.apicompare.vo.CompareResult;
import com.jykim.apicompare.vo.Parameter;
import com.jykim.apicompare.vo.RestResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * 두 API 결과 비교 프로그램
 * using Apache Lucene
 * @author jykim
 * @since 2018.03.08
 */
public class APICompareMain {


    public static void main(String[] args) {
        // Message
        StringBuilder message = new StringBuilder();
        message.append("########################\r\n");
        message.append("## API 호출 결과 비교 ##\r\n");
        message.append("########################");
        printProgramMessage(message.toString());

        // properties info
        PropertiesUtil propertiesUtil = PropertiesUtil.getInstance();
        ApiInfo apiInfo = propertiesUtil.getApiInfo();
        printProgramMessage(apiInfo.toString());

        // parsing Parameter File
        ParametersUtil parametersUtil = ParametersUtil.getInstance();
        List<Parameter> parameters = parametersUtil.getParameters();

        // call API
        printProgramMessage(">> Rest call API");
        RestServcie servcie = new RestServcie(apiInfo, parameters);
        List<RestResult> resultList = new ArrayList<>();
        try {
            long restCallStartTime = System.currentTimeMillis();
            resultList = servcie.doProcess();
            long restElapseTime = System.currentTimeMillis() - restCallStartTime;

            printProgramMessage(">> DONE("+String.valueOf(restElapseTime)+"ms)");
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (!resultList.isEmpty()) {
            printProgramMessage(">> Lucene Indexing ...");
            // create documents - two api result
            long indexingStartTime = System.currentTimeMillis();
            LuceneProcessing luceneProcessing = new LuceneProcessing(apiInfo, resultList);
            luceneProcessing.doIndexing();
            long indexingElapsedTie = System.currentTimeMillis() - indexingStartTime;
            printProgramMessage(">> Done("+String.valueOf(indexingElapsedTie)+"ms)");

            // todo compare two result
            printProgramMessage(">> Compare Two API Result...");
            long compareStartTime = System.currentTimeMillis();
            CompareResult compareResult = luceneProcessing.doCompare();
            long compareElapsedTime = System.currentTimeMillis() - compareStartTime;
            printProgramMessage(">> Done(" + String.valueOf(compareElapsedTime) + "ms)");

            float successRate = compareResult.getSuccessCount() / (float) compareResult.getTotalParamSize() * 100;

            List<Integer> failedParamSeq = compareResult.getFailedParamSeq();
            String failedParams = "";
            for (int i=0; i<failedParamSeq.size(); i++) {
                failedParams += failedParamSeq.get(i).toString();
                if (i < failedParamSeq.size()-1) {
                    failedParams += ", ";
                }
            }


            System.out.println();
            System.out.println("================================");
            System.out.println("성공률 : " + String.valueOf(successRate) +"%");
            System.out.println("실패 parameter no : " + failedParams);
        }



    }


    private static void printProgramMessage(String message) {
        System.out.println(message);
        System.out.println("\r\n");
        System.out.println("Please wait...");
        System.out.println("\r\n");
    }
}
