package com.jykim.apicompare.util;

import com.jykim.apicompare.vo.Parameter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * parameter.csv Util class
 * Singleton
 */
public class ParametersUtil {

    private static volatile ParametersUtil parametersUtil = new ParametersUtil();

    public static ParametersUtil getInstance() {
        if (parametersUtil == null) {
            synchronized (ParametersUtil.class) {
                if (parametersUtil == null) {
                    parametersUtil = new ParametersUtil();
                }
            }
        }
        return parametersUtil;
    }

    private List<Parameter> parameters;

    public ParametersUtil() {
        this.parameters = exportParameters();
    }

    private List<Parameter> exportParameters() {
        List<Parameter> parameterList = new ArrayList<>();
        InputStream is = getClass().getResourceAsStream("/parameter.csv");
        String line;
        String delimiter = ",";

        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                String[] field = line.split(delimiter);
                int lineNumber = Integer.parseInt(field[0]);
                String param = field[1].replaceAll("\\s","");
                Parameter parameter = new Parameter(lineNumber, param);
                parameterList.add(parameter);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return parameterList;
    }

    public List<Parameter> getParameters() {
        return this.parameters;
    }
}
