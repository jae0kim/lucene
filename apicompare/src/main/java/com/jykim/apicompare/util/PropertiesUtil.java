package com.jykim.apicompare.util;

import com.jykim.apicompare.vo.ApiInfo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * api.properties UTIL class
 * Singleton
 */
public class PropertiesUtil {

    private static volatile PropertiesUtil util = new PropertiesUtil();

    public static PropertiesUtil getInstance() {

        if(util == null){
            synchronized(PropertiesUtil.class){
                if(util == null){
                    util = new PropertiesUtil();
                }
            }
        }

        return util ;
    }

    private Properties properties;

    public PropertiesUtil() {
        this.properties = getProperties();
    }

    private Properties getProperties() {
        Properties props = new Properties();
        InputStream is = getClass().getResourceAsStream("/api.properties");
        try {
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }

    public ApiInfo getApiInfo() {
        String apiName1 = this.properties.getProperty("api1.name");
        String uri1 = this.properties.getProperty("api1.uri");
        String key1 = this.properties.getProperty("api1.key");
        String keyName1 = this.properties.getProperty("api1.keyName");

        String apiName2 = this.properties.getProperty("api2.name");
        String uri2 = this.properties.getProperty("api2.uri");
        String key2 = this.properties.getProperty("api2.key");
        String keyName2 = this.properties.getProperty("api2.keyName");

        ApiInfo apiInfo = new ApiInfo.ApiInfoBuilder(apiName1, uri1, apiName2, uri2)
                .setKeyName1(keyName1)
                .setKeyValue1(key1)
                .setKeyName2(keyName2)
                .setKeyValue2(key2)
                .build();

        return apiInfo;
    }
}
