package com.jykim.apicompare.rest;

import com.jykim.apicompare.vo.ApiInfo;
import com.jykim.apicompare.vo.Parameter;
import com.jykim.apicompare.vo.RestResult;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RestServcie {

    private ApiInfo apiInfo;
    private List<Parameter> parameters;

    public RestServcie(ApiInfo apiInfo, List<Parameter> parameters) {
        this.apiInfo = apiInfo;
        this.parameters = parameters;
    }

    public List<RestResult> doProcess() throws ExecutionException, InterruptedException {

        List<String> uriListAPI1 = makeURI(1, apiInfo.getKeyName1());
        List<String> uriListAPI2 = makeURI(2, apiInfo.getKeyName2());

        List<RestResult> allResult = new ArrayList<>();

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        for (int i=0; i<parameters.size(); i++) {

            Future<RestResult> restFuture1 = executorService.submit(new RestRequest(apiInfo.getApiName1(), parameters.get(i).getLineNumber(), uriListAPI1.get(i)));
            Future<RestResult> restFuture2 = executorService.submit(new RestRequest(apiInfo.getApiName2(), parameters.get(i).getLineNumber(), uriListAPI2.get(i)));

            RestResult restFutureResult1 = restFuture1.get();
            RestResult restFutureResult2 = restFuture2.get();

            allResult.add(restFutureResult1);
            allResult.add(restFutureResult2);

        }

        executorService.shutdown();

        return allResult;
    }

    private List<String> makeURI(int apiType, String keyName) {
        List<String> uriList = new ArrayList<>();
        String apiKey = null;
        String uri = null;

        if (apiType == 1) {
            apiKey = this.apiInfo.getKey1();
            uri = this.apiInfo.getUri1();
        } else if (apiType == 2) {
            apiKey = this.apiInfo.getKey2();
            uri = this.apiInfo.getUri2();
        } else {
            System.out.println("apiType mismatch!");
            System.exit(1);
        }

        try {
            String encodedKey = URLEncoder.encode(apiKey, "UTF-8");

            for (Parameter parameter : this.parameters) {
                StringBuilder callURI = new StringBuilder();
                callURI.append(uri);
                callURI.append("?"+keyName+"="+encodedKey);
                callURI.append("&"+parameter.getParameter());
                uriList.add(callURI.toString());
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return uriList;
    }
}
