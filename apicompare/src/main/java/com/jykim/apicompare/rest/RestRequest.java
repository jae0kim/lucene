package com.jykim.apicompare.rest;

import com.jykim.apicompare.vo.RestResult;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.concurrent.Callable;

/**
 * http rest request -> create lucene document
 * @author jykim
 */
public class RestRequest implements Callable{
    private String apiName;
    private int paramSeq;
    private String uri;

    public RestRequest(String apiName, int paramSeq, String uri) {
        this.apiName = apiName;
        this.paramSeq = paramSeq;
        this.uri = uri;
    }

    private RestResult httpGetRequest(String apiName, int paramSeq, String reqeustURI) {
        String result = null;
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet request= new HttpGet(reqeustURI);
        CloseableHttpResponse response;
        try {
            response = client.execute(request);
            HttpEntity entity = response.getEntity();

            result = EntityUtils.toString(entity, "UTF-8");

            EntityUtils.consume(entity);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (result.contains("error")) {
            System.out.println("[Error] : API 호출에 오류가 발생하였습니다");
            System.out.println(" - APINAME : " + apiName);
            System.out.println(" - paramSeq : " + paramSeq);
            System.out.println(" - requestURI : " + reqeustURI);

            System.exit(1);
        }

        RestResult restResult = new RestResult(apiName, paramSeq, result);

        return restResult;
    }


    @Override
    public Object call() throws Exception {
        return httpGetRequest(this.apiName, this.paramSeq, this.uri);
    }
}
