package com.jykim.apicompare.vo;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 파라미터 csv 객체
 * @author jykim
 */
public class Parameter {

    private int lineNumber;
    private String parameter;

    public Parameter(int lineNumber, String parameter) {
        this.lineNumber = lineNumber;
        this.parameter = parameter;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "lineNumber=" + lineNumber +
                ", parameter='" + parameter + '\'' +
                '}';
    }
}
