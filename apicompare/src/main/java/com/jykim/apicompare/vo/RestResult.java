package com.jykim.apicompare.vo;

public class RestResult {
    private String apiName;
    private int paramSeq;
    private String result;

    public RestResult(String apiName, int paramSeq, String result) {
        this.apiName = apiName;
        this.paramSeq = paramSeq;
        this.result = result;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public int getParamSeq() {
        return paramSeq;
    }

    public void setParamSeq(int paramSeq) {
        this.paramSeq = paramSeq;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
