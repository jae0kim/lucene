package com.jykim.apicompare.vo;

/**
 * api.properties mapping VO
 * @author jykim
 * @since 2018.03.08
 */
public class ApiInfo {

    private String apiName1;
    private String uri1;
    private String keyName1;
    private String key1;

    private String apiName2;
    private String uri2;
    private String keyName2;
    private String key2;

    private ApiInfo(ApiInfoBuilder builder) {
        this.apiName1 = builder.apiName1;
        this.uri1 = builder.uri1;
        this.keyName1 = builder.keyName1;
        this.key1 = builder.key1;

        this.apiName2 = builder.apiName2;
        this.uri2 = builder.uri2;
        this.keyName2 = builder.keyName2;
        this.key2 = builder.key2;
    }

    public String getApiName1() {
        return apiName1;
    }

    public String getUri1() {
        return uri1;
    }

    public String getKeyName1() {
        return keyName1;
    }

    public String getKey1() {
        return key1;
    }

    public String getApiName2() {
        return apiName2;
    }

    public String getUri2() {
        return uri2;
    }

    public String getKeyName2() {
        return keyName2;
    }

    public String getKey2() {
        return key2;
    }

    /**
     * ApiInfo Builder (Builder pattern)
     * 매개변수가 너무 많아져서 빌더 추가
     */
    public static class ApiInfoBuilder {
        private String apiName1;
        private String uri1;
        private String keyName1;
        private String key1;

        private String apiName2;
        private String uri2;
        private String keyName2;
        private String key2;

        public ApiInfoBuilder(String apiName1, String uri1, String apiName2, String uri2) {
            this.apiName1 = apiName1;
            this.uri1 = uri1;
            this.apiName2 = apiName2;
            this.uri2 = uri2;
        }

        public ApiInfoBuilder setKeyName1(String keyName1) {
            this.keyName1 = keyName1;
            return this;
        }

        public ApiInfoBuilder setKeyValue1(String key1) {
            this.key1 = key1;
            return this;
        }

        public ApiInfoBuilder setKeyName2(String keyName2) {
            this.keyName2 = keyName2;
            return this;
        }

        public ApiInfoBuilder setKeyValue2(String key2) {
            this.key2 = key2;
            return this;
        }

        public ApiInfo build() {
            return new ApiInfo(this);
        }

    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder();
        info.append("[API 1] : "+apiName1 + "\r\n");
        info.append("- uri : "+uri1+"\r\n");
        info.append("- keyName : "+keyName1+"\r\n");
        info.append("- keyValue : "+key1+"\r\n");
        info.append("[API 2] : "+apiName2 + "\r\n");
        info.append("- uri : "+uri2+"\r\n");
        info.append("- keyName : "+keyName2+"\r\n");
        info.append("- keyValue : "+key2+"\r\n");

        return info.toString();
    }
}
