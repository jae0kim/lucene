package com.jykim.apicompare.vo;

import java.util.List;

public class CompareResult {
    private int totalParamSize;
    private int successCount;
    private List<Integer> failedParamSeq;

    public CompareResult() {
    }

    public CompareResult(int totalParamSize, int successCount, List<Integer> failedParamSeq) {
        this.totalParamSize = totalParamSize;
        this.successCount = successCount;
        this.failedParamSeq = failedParamSeq;
    }

    public int getTotalParamSize() {
        return totalParamSize;
    }

    public void setTotalParamSize(int totalParamSize) {
        this.totalParamSize = totalParamSize;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    public List<Integer> getFailedParamSeq() {
        return failedParamSeq;
    }

    public void setFailedParamSeq(List<Integer> failedParamSeq) {
        this.failedParamSeq = failedParamSeq;
    }
}
