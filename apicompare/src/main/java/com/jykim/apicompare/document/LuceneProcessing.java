package com.jykim.apicompare.document;

import com.google.gson.*;
import com.jykim.apicompare.util.LuceneConstant;
import com.jykim.apicompare.vo.ApiInfo;
import com.jykim.apicompare.vo.CompareResult;
import com.jykim.apicompare.vo.RestResult;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Apache Lucene 처리
 * INDEX_PATH는 반드시 본인의 디렉토리에 맞게 변경할 것
 */
public class LuceneProcessing {

    private static final String INDEX_PATH = "/Users/gizmo/Documents/project_temp/lucene_index";
    private static final int HIT = 2;
    private ApiInfo apiInfo;
    private List<RestResult> resultList;


    public LuceneProcessing(ApiInfo apiInfo, List<RestResult> resultList) {
        this.apiInfo = apiInfo;
        this.resultList = resultList;
    }

    /**
     * 결과 -> Documents -> Indexing
     */
    public void doIndexing() {
        try {
            IndexWriter indexWriter = createWriter();
            List<Document> documents = new ArrayList<>();

            for (RestResult restResult : resultList) {
                documents.add(createDocument(restResult.getParamSeq(), restResult.getApiName(), restResult.getResult()));
            }

            // index 초기화 : 기존 인덱스 데이터 유지하려면 주석
            indexWriter.deleteAll();

            indexWriter.addDocuments(documents);
            indexWriter.commit();
            indexWriter.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * 인덱싱된 결과를 비교
     */
    public CompareResult doCompare() {
        CompareResult compareResult = new CompareResult();
        try {
            IndexSearcher  searcher = createSearcher();

            Set<Integer> paramSeqSet = new HashSet<>();
            for (RestResult restResult : resultList) {
                paramSeqSet.add(restResult.getParamSeq());
            }

            List<Integer> failedParamSeq = new ArrayList<>();
            int totalResultSize = paramSeqSet.size();
            int successCount = 0;

            for (int paramSeq : paramSeqSet) {
                boolean isEqual = indivisualCompare(paramSeq, searcher);
                if (isEqual) {
                    successCount++;
                } else {
                    failedParamSeq.add(paramSeq);
                }
            }

            compareResult = new CompareResult(totalResultSize, successCount, failedParamSeq);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return compareResult;
    }

    /**
     * Ducment(Row) 생성
     * @param paramSeq 파라미터 No
     * @param apiName api name
     * @param result api 호출 결과
     * @return
     */
    private Document createDocument(Integer paramSeq, String apiName, String result) {

        Document doc = new Document();
        doc.add(new StringField(LuceneConstant.seq.toString(), paramSeq.toString(), Field.Store.YES));
        doc.add(new StringField(LuceneConstant.apiName.toString(), apiName, Field.Store.YES));
        doc.add(new TextField(LuceneConstant.result.toString(), result, Field.Store.YES));

        return doc;
    }

    /**
     * Index 생성/관리
     * @return
     * @throws IOException
     */
    private IndexWriter createWriter () throws IOException {
        FSDirectory fsd = FSDirectory.open(Paths.get(INDEX_PATH));
        IndexWriterConfig config = new IndexWriterConfig(new StandardAnalyzer());
        IndexWriter indexWriter = new IndexWriter(fsd, config);
        return indexWriter;
    }

    /**
     * paramSeq와 apiName으로 Searching
     * @param paramSeq
     * @param searcher
     * @return
     * @throws ParseException
     * @throws IOException
     */
    private TopDocs searchByParamSeq(Integer paramSeq, String apiName, IndexSearcher searcher) throws Exception {

        QueryParser queryParser1 = new QueryParser(LuceneConstant.seq.toString(), new StandardAnalyzer());
        Query idQuery1 = queryParser1.parse(paramSeq.toString());

        QueryParser queryParser2 = new QueryParser(LuceneConstant.apiName.toString(), new StandardAnalyzer());
        Query idQuery2 = queryParser2.parse(apiName);

        BooleanQuery finalQuery = new BooleanQuery.Builder()
                .add(idQuery1, BooleanClause.Occur.MUST)
                .add(idQuery2, BooleanClause.Occur.MUST)
                .build();


        TopDocs hits = searcher.search(finalQuery, HIT);
        return hits;
    }



    /**
     * Index Searcher 생성
     * @return
     * @throws IOException
     */
    private static IndexSearcher createSearcher() throws IOException {
        Directory dir = FSDirectory.open(Paths.get(INDEX_PATH));
        IndexReader reader = DirectoryReader.open(dir);
        IndexSearcher searcher = new IndexSearcher(reader);
        return searcher;
    }

    /**
     * api1, api2 비교
     * @param paramSeq
     * @param searcher
     * @return
     * @throws Exception
     */
    private boolean indivisualCompare(int paramSeq, IndexSearcher searcher) throws Exception {

        TopDocs topDocs1 = searchByParamSeq(paramSeq, this.apiInfo.getApiName1(), searcher);
        TopDocs topDocs2 = searchByParamSeq(paramSeq, this.apiInfo.getApiName2(), searcher);

        ScoreDoc scoreDoc1 = topDocs1.scoreDocs[0];
        ScoreDoc scoreDoc2 = topDocs2.scoreDocs[0];

        Document doc1 = searcher.doc(scoreDoc1.doc);
        Document doc2 = searcher.doc(scoreDoc2.doc);

        String result1 = String.format(doc1.get(LuceneConstant.result.toString()));
        String result2 = String.format(doc2.get(LuceneConstant.result.toString()));

        JsonElement jsonElement1 = new JsonParser().parse(result1);
        JsonElement jsonElement2 = new JsonParser().parse(result2);

        boolean result = compareJsonObject(jsonElement1, jsonElement2);
        return result;
    }

    /**
     * json 객체 비교
     * 주의 : JsonArray의 경우 순서도 반드시 같아야함. => 개선 필요
     * @param jsonElement1
     * @param jsonElement2
     * @return
     */
    private boolean compareJsonObject(JsonElement jsonElement1, JsonElement jsonElement2) {

        boolean isEqual = true;

        if (jsonElement1 != null && jsonElement2 != null) {
            if (jsonElement1.isJsonObject() && jsonElement2.isJsonObject()) {
                Set<Map.Entry<String, JsonElement>> entrySet1 = ((JsonObject) jsonElement1).entrySet();
                Set<Map.Entry<String, JsonElement>> entrySet2 = ((JsonObject) jsonElement2).entrySet();

                JsonObject jsonObject2 = (JsonObject) jsonElement2;

                if (entrySet1 != null && entrySet2 != null && (entrySet1.size() == entrySet2.size())) {
                    for (Map.Entry<String, JsonElement> entry : entrySet1) {
                        isEqual = isEqual && compareJsonObject(entry.getValue(), jsonObject2.get(entry.getKey()));
                    }
                } else {
                    return false;
                }
            } else if (jsonElement1.isJsonArray() && jsonElement2.isJsonArray()) {
                JsonArray jsonArray1 = jsonElement1.getAsJsonArray();
                JsonArray jsonArray2 = jsonElement2.getAsJsonArray();

                if (jsonArray1.size() == jsonArray2.size()) {
                    for (int i=0; i<jsonArray1.size(); i++) {
                        JsonElement jsonElement = jsonArray1.get(i);
                        isEqual = isEqual && compareJsonObject(jsonElement, jsonArray2.get(i));
                    }

                } else {
                    return false;
                }
            } else if (jsonElement1.isJsonPrimitive() && jsonElement2.isJsonPrimitive()) {
                if (jsonElement1.equals(jsonElement2)) {
                    return true;
                } else {
                    return false;
                }
            } else if (jsonElement1.isJsonNull() && jsonElement2.isJsonNull()) {
                return true;
            } else if (jsonElement1 == null && jsonElement2 == null) {
                return true;
            } else {
                return false;
            }
        }
        return isEqual;
    }
}
