package sample.lucene;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;

public class Searching {

    private static final String INDEX_PATH = "/Users/gizmo/Documents/project_temp/lucene_index";

    public static void main(String[] args) {
        try {
            IndexSearcher searcher = createSearcher();

            TopDocs foundDocs = searchByID(1, searcher);
            System.out.println("Total Result : "+ foundDocs.totalHits);

            for (ScoreDoc sd : foundDocs.scoreDocs) {
                Document d = searcher.doc(sd.doc);
                System.out.println(String.format(d.get("firstName")));
            }


            TopDocs foundDocs2 = searchByFirstName("Brian", searcher);
            System.out.println("Total Result : "+ foundDocs2.totalHits);

            for (ScoreDoc sd : foundDocs2.scoreDocs) {
                Document d = searcher.doc(sd.doc);
                System.out.println(String.format(d.get("id")));
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static TopDocs searchByFirstName(String firstName, IndexSearcher searcher) throws Exception {
        QueryParser queryParser = new QueryParser("firstName", new StandardAnalyzer());
        Query firstNameQuery = queryParser.parse(firstName);
        TopDocs hits = searcher.search(firstNameQuery, 10);
        return hits;
    }

    private static TopDocs searchByID(Integer id, IndexSearcher searcher) throws Exception {
        QueryParser queryParser = new QueryParser("id", new StandardAnalyzer());
        Query idQuery = queryParser.parse(id.toString());
        TopDocs hits = searcher.search(idQuery, 10);
        return hits;
    }

    private static IndexSearcher createSearcher() throws IOException {
        Directory dir = FSDirectory.open(Paths.get(INDEX_PATH));
        IndexReader reader = DirectoryReader.open(dir);
        IndexSearcher searcher = new IndexSearcher(reader);
        return searcher;
    }
}
